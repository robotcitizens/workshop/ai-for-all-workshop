import cv2
from pyzbar import pyzbar

is_detect = False
  
def BarcodeReader(image):
     
    detectedBarcodes = pyzbar.decode(image)
      
    if not detectedBarcodes:
        # print("Barcode Not Detected or your barcode is blank/corrupted!")
        pass
    else:
        for barcode in detectedBarcodes: 
            (x, y, w, h) = barcode.rect
            cv2.rectangle(image, (x-10, y-10),
                          (x + w+10, y + h+10),
                          (255, 0, 0), 2)
             
            if barcode.data!="":
                barcode_data = barcode.data.decode()
                print(barcode_data)
                # print(barcode.type)
                global is_detect
                is_detect = True

    cv2.imshow("Image", image)
    cv2.waitKey(1)
 
if __name__ == "__main__":
    ret = False
    cap = cv2.VideoCapture(0)
    while not is_detect:
        ret, frame = cap.read()
        BarcodeReader(frame)
    else:
        cv2.destroyAllWindows()