import cv2
from pyzbar import pyzbar

count = 0
price = 0
product_dict = {"8856976000016":120, "423542334":50, "432432879":100}
  
def BarcodeReader(image):
     
    detectedBarcodes = pyzbar.decode(image)
      
    if not detectedBarcodes:
        # print("Barcode Not Detected or your barcode is blank/corrupted!")
        pass
    else:
        for barcode in detectedBarcodes: 
            (x, y, w, h) = barcode.rect
            cv2.rectangle(image, (x-10, y-10),
                          (x + w+10, y + h+10),
                          (255, 0, 0), 2)
             
            if barcode.data!="":
                barcode_data = barcode.data.decode()
                print(barcode_data)
                # print(barcode.type)
                global price, count
                if barcode_data in product_dict.keys():
                    price += product_dict[barcode_data]
                    count += 1
                    input("Press ENTER to scan another product.")
                else:
                    print("Can't find product")
                
    cv2.imshow("Image", image)
    cv2.waitKey(1)
 
if __name__ == "__main__":
    ret = False
    cap = cv2.VideoCapture(0)
    cap.set(cv2.CAP_PROP_BUFFERSIZE, 1)
    while count < 3:
        ret, frame = cap.read()
        ret, frame = cap.read()
        BarcodeReader(frame)
    else:
        cv2.destroyAllWindows()
    print(f"Total price : {price}")